import React, { useState, useEffect } from 'react';
import { View, Text, FlatList, Image, StyleSheet, TouchableOpacity, ScrollView } from 'react-native';
import { Container, Content, Icon, Card, CardItem, Body, ListItem } from 'native-base';
import DetailMenuHeader from '../components/detailMenuComponent/DetailMenuHeader';
import { RedButton, AppIndicator } from '../utils/commons';
import { width } from '../utils/constants';
import PropTypes from 'prop-types';
import { imageSource } from '../utils/pureFunction';
import { fetchChurchs } from '../utils/api';
import OpenLinking from '../utils/OpenLinking';
import Modal from 'react-native-modal';
import Footer from '../task/Footer';
import HeaderTitle from '../components/HeaderTitle';
import { useTranslation } from 'react-i18next';

let contentComponent = null;

const Info = ({ icon, image, title, subtitle, button, onPress, backgroundColor, }) => {
    return (
        <View style={{ padding: 9, backgroundColor, width: '100%' }}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                {icon && (
                    <Icon type={'FontAwesome'} name={icon} style={{ color: 'rgb(229, 46, 45)', fontSize: 25 }} />
                )}
                {image && (
                    <Image source={image} style={{ width: 20, height: 25, backgroundColor: 'white' }} />
                )}
                <Text style={{ color: '#333333', marginLeft: 10, fontSize: 16.2, fontWeight: '500' }}>{title}</Text>
            </View>
            <Text
                onPress={onPress}
                style={
                    [
                        button ? { color: '#007bff' } : { color: 'rgba(51, 51, 51, 0.7)' },
                        { fontSize: 16.2 }
                    ]
                }
            >{subtitle}</Text>
        </View>
    )
};

Info.propTypes = {
    icon: PropTypes.string,
    title: PropTypes.string.isRequired,
    subtitle: PropTypes.string,
    button: PropTypes.bool,
    onPress: PropTypes.func,
}

export default function ChurchListScreen({ navigation }) {

    const [state, setState] = useState({
        churchList: [],
        loading: false,
        error: false,
        searchFormOpen: false,
        english_ceremony: false,
        vietnamese_ceremony: false,
        saturday_ceremony: false,
        sunday_am: false,
        sunday_pm: false,
    });

    const { t } = useTranslation();

    const {
        churchList,
        loading,
        error,
        searchFormOpen,
        english_ceremony,
        vietnamese_ceremony,
        saturday_ceremony,
        sunday_am,
        sunday_pm
    } = state;

    useEffect(() => {
        getData();
    }, []);

    getData = async (query = null) => {
        setState({
            ...state,
            loading: true,
        })
        try {
            const data = await fetchChurchs(query);
            setState({
                ...state,
                loading: false,
                error: false,
                churchList: data.data,
                searchFormOpen: false,
            });

            if (contentComponent) {
                setTimeout(() => {
                    contentComponent._root.scrollToPosition(0, 0)
                })
            }
        }
        catch (e) {
            setState({
                ...state,
                loading: false,
                error: true,
            })
        }

    };

    if (loading) {
        return AppIndicator
    }

    renderItem = ({ item }) => {
        return (
            <TouchableOpacity onPress={() => navigation.navigate('Church', { data: item })} activeOpacity={0.8}>
                <Card style={{ paddingVertical: 27 }}>
                    <CardItem>
                        <Body style={{ justifyContent: 'space-around', }}>
                            <Image source={imageSource(item.thumbnail_url400x)} style={{ width: '100%', height: width, }} />
                            <Text style={styles.title}>{item.title}</Text>

                            {/* Address */}
                            <Info
                                icon={'map-marker'}
                                title={t('address')}
                                subtitle={item.address}
                                button

                                onPress={() => navigation.navigate('Church', { data: item, map_only: true })}
                            />

                            {/* Misa By english */}
                            <Info
                                image={require('../assets/images/english-ico.png')}
                                title={t('misaByEnglish')}
                                subtitle={item.english_ceremony ? 'Có' : 'Không'}
                                backgroundColor={'rgba(51, 51, 51, 0.05)'}
                            />

                            {/* Misa By Vietnamese */}
                            <Info
                                icon={'vine'}
                                title={t('misaByVietnamese')}
                                subtitle={item.vietnamese_ceremony ? 'Có' : 'Không'}
                            />

                            {/* Sunday Misa */}
                            <Info
                                image={require('../assets/images/cross-ico.png')}
                                title={t('misaSunday')}
                                subtitle={item.sunday_ceremony}
                                backgroundColor={'rgba(51, 51, 51, 0.05)'}

                            />

                            {/* Weekday Misa */}
                            <Info
                                image={require('../assets/images/cross-ico.png')}
                                title={t('misaWeekday')}
                                subtitle={item.normal_day_ceremony}
                            />

                            {/* Website */}
                            {item.website && (
                                <Info
                                    icon={'globe'}
                                    title={'Website'}
                                    subtitle={item.website}
                                    onPress={() => OpenLinking(item.website)}
                                    button
                                    backgroundColor={'rgba(51, 51, 51, 0.05)'}
                                />
                            )}


                            {/* Source */}
                            <Info
                                icon={'table'}
                                title={t('source')}
                                subtitle={'http://tokyo.catholic.jp'}
                                onPress={() => OpenLinking('http://tokyo.catholic.jp')}
                                button
                            />

                        </Body>
                    </CardItem>
                </Card>
            </TouchableOpacity>

        )

    };

    renderTop = () => {
        return (
            <DetailMenuHeader
                postingNumber={churchList.length}
            />
        )
    };

    openSearchForm = () => {
        setState({
            ...state,
            searchFormOpen: true,
        })
    };

    handleCloseModal = () => {
        setState({
            ...state,
            searchFormOpen: false,
        })
    };

    toggleChecked = name => {

        setState({
            ...state,
            [name]: !state[name]
        });

    };

    doSearch = () => {
        setState({
            ...state,
            searchFormOpen: false,
        });

        let query = {};

        for (const stateName in state) {
            if (state.hasOwnProperty(stateName)) {
                const value = state[stateName]
                if (value) {
                    query[stateName] = 1
                }
            }
        };

        getData(query);
    };

    renderFooter = () => {
        return (
            <View>

                {state.error && (
                    <View style={styles.center}>
                        <Text>Error</Text>
                    </View>
                )}

                <Footer />
            </View>
        )
    };

    return (
        <Container>

            <Content
                style={{ flex: 1, }}
                contentContainerStyle={{ flex: 1, }}
                ref={c => contentComponent = c}
            >
                {churchList.length === 0 && (
                    <View style={styles.noArticles}>
                        <Text style={{ fontSize: 20 }}>Chưa có bài viết nào</Text>
                    </View>
                )}

                {churchList.length !== 0&& (
                    <FlatList
                        data={churchList}
                        renderItem={renderItem}
                        keyExtractor={(item, index) => index.toString()}
                        ListHeaderComponent={renderTop}
                        ListFooterComponent={renderFooter}
                        initialNumToRender={3}
                    />
                )}

                <RedButton rounded style={styles.searchButton} onPress={openSearchForm}>
                    <Icon type="FontAwesome" name="search" />
                </RedButton>

                <Modal
                    isVisible={searchFormOpen}
                    animationOut="slideOutDown"
                    onBackButtonPress={handleCloseModal}
                    onBackdropPress={handleCloseModal}
                    style={styles.modal}
                    useNativeDriver={true}
                    scrollHorizontal={false}
                    children={true}
                >
                    <View style={styles.modalContainer}>
                        <ScrollView>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text style={styles.redTitle}>Tìm kiếm nhà thờ</Text>
                                <TouchableOpacity
                                    onPress={handleCloseModal}>
                                    <Icon name="close" />
                                </TouchableOpacity>
                            </View>

                            <View style={{ marginTop: 20 }}>
                                <Text style={{ fontSize: 20, paddingBottom: 2 }}>Ngôn ngữ</Text>
                            </View>

                            <ListItem noIndent noBorder onPress={() => toggleChecked('english_ceremony')} style={styles.row}>
                                <Icon style={styles.checkbox} type="FontAwesome" name={english_ceremony ? 'check-square-o' : 'square-o'} />
                                <Text>Lễ bằng tiếng Anh</Text>
                            </ListItem>

                            <ListItem noIndent noBorder onPress={() => toggleChecked('vietnamese_ceremony')} style={styles.row}>
                                <Icon style={styles.checkbox} type="FontAwesome" name={vietnamese_ceremony ? 'check-square-o' : 'square-o'} />
                                <Text>Lễ bằng tiếng Việt</Text>
                            </ListItem>

                            <View style={{ marginTop: 20 }}>
                                <Text style={{ fontSize: 20, paddingBottom: 2 }}>Giờ lễ</Text>
                            </View>

                            <ListItem noIndent noBorder onPress={() => toggleChecked('saturday_ceremony')} style={styles.row}>
                                <Icon style={styles.checkbox} type="FontAwesome" name={saturday_ceremony ? 'check-square-o' : 'square-o'} />
                                <Text>Lễ thứ bảy</Text>
                            </ListItem>

                            <ListItem noIndent noBorder onPress={() => toggleChecked('sunday_am')} style={styles.row}>
                                <Icon style={styles.checkbox} type="FontAwesome" name={sunday_am ? 'check-square-o' : 'square-o'} />
                                <Text>Lễ chúa nhật buổi sáng</Text>
                            </ListItem>

                            <ListItem noIndent noBorder onPress={() => toggleChecked('sunday_pm')} style={styles.row}>
                                <Icon style={styles.checkbox} type="FontAwesome" name={sunday_pm ? 'check-square-o' : 'square-o'} />
                                <Text>Lễ chúa nhật buổi chiều</Text>
                            </ListItem>

                            <View style={styles.flexRowCenter}>

                                <RedButton rounded onPress={doSearch} style={styles.button}>
                                    <Text style={styles.searchText}>Tìm kiếm</Text>
                                </RedButton>

                            </View>
                        </ScrollView>
                    </View>
                </Modal>

            </Content>
        </Container>
    );
}

ChurchListScreen.navigationOptions = ({ navigation }) => {
    const type = 'church'
    return {
        headerTitle: <HeaderTitle category={type ? type : order} readMore />,
    }
}

const styles = StyleSheet.create({
    title: {
        color: "#333333",
        fontSize: 22.5,
        marginVertical: 10,
        fontWeight: '500',
    },

    center: {
        justifyContent: 'center',
        alignItems: 'center'
    },

    noArticles: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },

    searchButton: {
        position: 'absolute',
        bottom: 30,
        right: 20,
    },

    modalContainer: {
        backgroundColor: 'rgb(255, 255, 255)',
        padding: 20,
        bottom: -20,
        right: -20,
        left: -20,
        position: 'absolute',
    },

    row: {
        flexDirection: 'row',
    },

    checkbox: {
        marginRight: 10,
    },

    redTitle: {
        fontSize: 22,
        color: '#e73227',
    },

    searchText: {
        fontSize: 18,
        color: '#FFFFFF',
        textAlign: 'center',
    },

    flexRowCenter: {
        justifyContent: 'center',
    },

    button: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    }

});
