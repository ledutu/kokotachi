import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { StyleSheet, FlatList, Image, TouchableOpacity, Text, ActivityIndicator } from 'react-native'
import { View, Container, Content, ListItem, Body, Button, Icon, Input, Picker, Item, Card, CardItem } from 'native-base'
import { format, parseISO } from 'date-fns';
import Modal from 'react-native-modal'
import styles from '../styles'
import { fetchEvents } from '../utils/api'
import ScalableImage from 'react-native-scalable-image'
import { imageSource } from '../utils/pureFunction'
import { PangolinText, PangolinLabel, AppIndicator, RedButton, CustomPicker, RedTitle } from '../utils/commons'
import { useTranslation } from 'react-i18next'
import { type_utils, width } from '../utils/constants'
import DateTimePicker from "react-native-modal-datetime-picker";
import DetailMenuHeader from '../components/detailMenuComponent/DetailMenuHeader';
import Footer from '../task/Footer';
import { navigate, pushNavigate } from '../utils/navigation';



const SearchForm_MapStateToProps = state => ({
    prefectures: state.prefectures,
    eventTypes: state.eventTypes
})

/**
 * SearchForm component, connected with store to get `prefectures`
 * 
 * @returns {ConnectedComponentClass}
 */


const SearchForm = connect(SearchForm_MapStateToProps)(function ({
    prefectures,
    eventTypes,
    isVisible,
    closeModal,
    search
}) {
    const { t } = useTranslation()
    const [state, setState] = useState({
        keyword: null,
        event_id: null,
        prefecture_id: null,
        category_slug: null,
        isDateTimePickerVisible: false
    })
    const [statusState, setStatusState] = useState({
        searching: false
    })
    const _styles = StyleSheet.create({
        container: {
            backgroundColor: 'rgb(255, 255, 255)',
            padding: 20,
            bottom: -20,
            right: -20,
            left: -20,
            position: 'absolute'
        },
        row: {
            flexDirection: 'row'
        },
        checkbox: {
            marginRight: 10
        }
    })

    const doSearch = () => {
        setStatusState({
            ...statusState,
            searching: true
        })
        let query = {}
        for (const state_name in state) {
            if (state.hasOwnProperty(state_name)) {
                const value = state[state_name]
                if (value) {
                    query[state_name] = value
                }
            }
        }

        search(page = 1, query);

        setState({
            ...state,
            keyword: null,
            event_id: null,
            prefecture_id: null,
            category_slug: null,
        })
    }

    showDateTimePicker = () => {
        setState({
            ...state,
            isDateTimePickerVisible: true
        })
    };

    hideDateTimePicker = () => {
        setState({
            ...state,
            isDateTimePickerVisible: false
        })
    };

    handleDatePicked = date => {
        console.log("A date has been picked: ", date);
        hideDateTimePicker();
    };

    useEffect(() => {
        if (!isVisible) {
            setStatusState({ ...statusState, searching: false })
        }
    }, [isVisible])

    return (
        <Modal
            animationInTiming={100}
            animationOutTiming={100}
            isVisible={isVisible}
            onBackdropPress={closeModal}
            onBackButtonPress={closeModal}>
            <View style={_styles.container}>
                <RedTitle>{t('searchEvent')}</RedTitle>

                <Item stackedLabel>
                    <PangolinLabel>{t('keyword')}</PangolinLabel>
                    <Input
                        value={state.keyword}
                        onChangeText={keyword => setState({ ...state, keyword })}
                        placeholder={t('enterKeyword')}
                        placeholderTextColor="#ccc" />
                </Item>

                <Item stackedLabel>
                    <PangolinLabel>{t('eventId')}</PangolinLabel>
                    <Input
                        value={state.event_id}
                        onChangeText={event_id => setState({ ...state, event_id })}
                        placeholder={t('enterEventId')}
                        placeholderTextColor="#ccc" />
                </Item>

                <View style={styles.flexRowCenter}>
                    <RedButton rounded onPress={showDateTimePicker}>
                        <PangolinText>{t('showDatePicker')}</PangolinText>
                    </RedButton>
                </View>

                <DateTimePicker
                    isVisible={state.isDateTimePickerVisible}
                    onConfirm={handleDatePicked}
                    onCancel={hideDateTimePicker}
                />

                <View style={{ marginTop: 10 }}>
                    <PangolinLabel>{t('prefecture')}</PangolinLabel>
                    <CustomPicker
                        placeholder={t('prefecture')}
                        selectedValue={state.prefecture_id}
                        onValueChange={prefecture_id => setState({ ...state, prefecture_id })}
                    >
                        <Picker.Item key={-1} label={t('choosePrefecture')} color="#ccc" value="" />
                        {prefectures.map(prefecture => (
                            <Picker.Item key={prefecture.id} label={prefecture.title} value={prefecture.id} />
                        ))}
                    </CustomPicker>
                </View>

                {/* Type event */}
                <View style={{ marginTop: 10 }}>
                    <PangolinLabel>{t('typeEvent')}</PangolinLabel>
                    <CustomPicker
                        placeholder={t('typeEvent')}
                        selectedValue={state.category_slug}
                        onValueChange={category_slug => setState({ ...state, category_slug })}
                    >
                        <Picker.Item key={-1} label={t('chooseTypeEvent')} color="#ccc" value="" />
                        {eventTypes.map(item => (
                            <Picker.Item key={item.slug} label={item.title} value={item.slug} />
                        ))}
                    </CustomPicker>
                </View>

                <View style={styles.flexRowCenter}>
                    {!statusState.searching ? (
                        <RedButton rounded onPress={doSearch}>
                            <PangolinText>{t('search')}</PangolinText>
                        </RedButton>
                    ) : (
                            <PangolinText>{t('searching') + '...'}</PangolinText>
                        )}
                </View>
            </View>
        </Modal>
    )
})

const renderEventItem = ({ item }) => {
    return (
        <TouchableOpacity onPress={() => navigate('Event', { data: item })} activeOpacity={0.8}>
            <Card style={{ paddingVertical: 27 }}>
                <CardItem>
                    <Body>
                        <ScalableImage source={imageSource(item.image_path)} width={(width - 35)} />
                        <Text style={_styles.title}>{item.article.title}</Text>

                        {item.categories &&
                            <View style={{ flexDirection: 'row', paddingVertical: 3.6 }}>
                                <View style={{ flex: 3 }}>
                                    <Text style={[{ alignSelf: 'flex-start' }, _styles.eventType]}>Loại sự kiện</Text>
                                </View>

                                <View style={{ color: 'rgba(51, 51, 51, 0.3)', flex: 5 }}>
                                    {item.categories.map((one) => {
                                        let { title, slug, type } = one;

                                        return (
                                            <TouchableOpacity
                                                key={one.id}
                                                style={{ marginBottom: 10 }}
                                                onPress={() => { pushNavigate('EventList', { title, type, slug }) }}>
                                                <Text key={one.id} style={{ textDecorationLine: 'underline' }}>{title}</Text>
                                            </TouchableOpacity>
                                        )
                                    })}
                                </View>
                            </View>
                        }

                        <View style={{ flexDirection: 'row', paddingVertical: 3.6 }}>
                            <Text style={[{ marginRight: 10, flex: 3 }, _styles.eventType]}>Ngày bắt đầu</Text>
                            <Text style={{ color: 'rgba(51, 51, 51, 0.3)', flex: 5 }}>{format(parseISO(item.started_at), 'yyyy-MM-dd')}</Text>
                        </View>
                        {item.prefecture && (
                            <View style={{ flexDirection: 'row', paddingVertical: 3.6 }}>
                                <Text style={[{ marginRight: 10, flex: 3 }, _styles.eventType]}>Tỉnh</Text>
                                <Text style={{ color: 'rgba(51, 51, 51, 0.3)', flex: 5 }}>{item.prefecture.prefecture_name_english}</Text>
                            </View>
                        )}

                        <View style={{ paddingTop: 0, paddingVertical: 9 }}>
                            <Text note style={_styles.eventType}>{item.summary}</Text>
                        </View>
                    </Body>
                </CardItem>
            </Card>
        </TouchableOpacity>
    )

}

const eventListScreenStyles = StyleSheet.create({
    searchButton: {
        position: 'absolute',
        bottom: 20,
        right: 20,
    }
})

let contentComponent = null

export default function EventListScreen({ navigation }) {
    const { t } = useTranslation()
    const [state, setState] = useState({
        events: [],
        paginated: null,
        loadMoreDisabled: false,
        searchFormOpening: false,
        no_query: true,
        error: false,
    })
    const category_slug = navigation.getParam('slug')
    const title = navigation.getParam('title')
    const { events, paginated, loadMoreDisabled, searchFormOpening, error } = state

    async function getEvents(page = 1, query = null) {
        setState({
            ...state,
            loadMoreDisabled: true,
        })
        try {
            const response = await fetchEvents({ category_slug, page }, query);
            const _events = response.data._data.events.data;

            setState({
                ...state,
                events: page == 1 ? _events : events.concat(_events),
                paginated: response.data._data.events,
                loadMoreDisabled: false,
                searchFormOpening: false,
                no_query: query ? false : true,
                error: false,
            })
            if (contentComponent && page == 1) {
                setTimeout(() => {
                    contentComponent._root.scrollToPosition(0, 0)
                })
            }
        } catch (e) {
            console.log("Error fetching events", e);
            setState({
                ...state,
                error: true,
            })
        };

    }

    renderTop = () => {
        return (
            <DetailMenuHeader
                postingNumber={paginated.total}
            />
        )
    };

    renderFooter = () => {
        return (
            <View>

                {error && (
                    <View style={_styles.center}>
                        <Text>{t('error')}</Text>
                    </View>
                )}

                {paginated.current_page >= paginated.last_page && (
                    <View style={[_styles.center, { marginVertical: 10 }]}>
                        <Text>Bạn đã ở cuối bài viết</Text>
                    </View>
                )}

                {paginated.current_page < paginated.last_page && (
                    <ListItem noBorder>
                        <Body style={{ flexDirection: 'row', justifyContent: 'center' }}>
                            <Button disabled={loadMoreDisabled} rounded bordered small onPress={() => getEvents(paginated.current_page + 1)}>
                                <PangolinText>{t('readMore')}</PangolinText>
                            </Button>
                        </Body>
                    </ListItem>
                )}

                <Footer />
            </View>
        )
    };

    // like `componentDidMount`
    useEffect(() => {
        getEvents()
    }, [])

    if (!paginated) {
        return AppIndicator
    }

    const openSearchForm = () => setState({ ...state, searchFormOpening: true })
    const closeSearchForm = () => setState({ ...state, searchFormOpening: false })

    return (
        <Container>
            <Content
                ref={c => contentComponent = c}
                contentContainerStyle={{ flex: 1, }}
                style={{ flex: 1 }}
            >

                {paginated.total === 0 && (
                    <View style={{ alignItems: 'center', flex: 1, justifyContent: 'center', }}>
                        <Text style={{ fontSize: 20}}>Chưa có bài viết nào</Text>
                    </View>
                )}

                {paginated.total !== 0 && (
                    <FlatList
                        data={events}
                        initialNumToRender={3}
                        keyExtractor={(item) => item.id.toString()}
                        renderItem={renderEventItem}
                        ListHeaderComponent={renderTop}
                        ListFooterComponent={renderFooter} />
                )}

            </Content>

            <SearchForm isVisible={searchFormOpening} closeModal={closeSearchForm} search={getEvents} />

            <RedButton rounded style={eventListScreenStyles.searchButton} onPress={openSearchForm}>
                <Icon type="FontAwesome" name="search" />
            </RedButton>
        </Container>
    )
}

EventListScreen.navigationOptions = ({ navigation }) => {
    const title = navigation.getParam('title');
    const type = navigation.getParam('type');

    return {
        headerRight: <View />,
        headerTitle: <Title type={type ? type : ""} title={title} readMore={!title ? true : false} />,
    }
};

const Title = ({ type, title, navigation, readMore }) => {
    const { t } = useTranslation();

    if (readMore) {
        return (
            <View style={[{ flexDirection: 'row', }]}>
                <Image source={type_utils.event.icon} resizeMode="contain" style={[Platform.OS === 'ios' && { marginBottom: 10 }, _styles.titleImage]} />

                <View style={{ flexDirection: 'column', justifyContent: 'center', }}>
                    <TouchableOpacity activeOpacity={1}>
                        <PangolinText style={_styles.typeDisplay}>{t(`type.${type_utils.event.normalDisplay}`)}</PangolinText>
                    </TouchableOpacity>
                </View>
            </View>
        )
    };

    return (
        <View style={[{ flexDirection: 'row' }]}>
            <Image source={type_utils.event.icon} resizeMode="contain" style={[Platform.OS === 'ios' && { marginBottom: 10 }, _styles.titleImage]} />

            <View style={{ flexDirection: 'column', justifyContent: 'center' }}>
                {/* // onPress={() => navigation.push('DetailMore', { type, title: t(type_utils[type].display) })} */}

                <TouchableOpacity activeOpacity={1} >
                    <PangolinText style={_styles.typeDisplay}>{t(`type.${type_utils.event.normalDisplay}`)}</PangolinText>
                </TouchableOpacity>

                {(title !== ('Event')) && (
                    //  onPress={() => navigation.push('DetailMore', { slug, type: category.type, title })}
                    <TouchableOpacity activeOpacity={1}>
                        <PangolinText style={_styles.categoryTitle}>{title}</PangolinText>
                    </TouchableOpacity>
                )}

            </View>
        </View>
    )
};

const _styles = StyleSheet.create({
    typeDisplay: {
        fontSize: 20,
        textAlign: 'center',
    },
    categoryTitle: {
        fontSize: 16,
    },
    titleImage: {
        height: 33,
        marginTop: 10
    },
    title: {
        fontSize: 22.5,
        fontWeight: '500',
        marginVertical: 18,
        color: '#333333',
    },

    eventType: {
        fontSize: 16.2,
        color: '#333333',
    },

    center: {
        justifyContent: 'center',
        alignItems: 'center',
    },

    searchButton: {
        position: 'absolute',
        bottom: 30,
        right: 20,
    },

    container: {
        backgroundColor: 'rgb(255, 255, 255)',
        padding: 20,
        bottom: -20,
        right: -20,
        left: -20,
        position: 'absolute'
    },

});