import React, { useState, useEffect } from 'react';
import { StyleSheet } from 'react-native';
import AppHeader from '../task/Header';
import Footer from '../task/Footer';
import DoubleRow from '../components/DoubleRow';
import ChurchInRow from '../components/church/ChurchInRow';
import {
    fetchArticles,
    fetchBanners,
    fetchTopChurch,
    fetchEvents,
} from '../utils/api';
import { connect } from 'react-redux';
import store from '../store';
import { setPrimary, setActicles, resetAll } from '../actions/home';

import Banners from '../task/Banners';
import { type_utils } from '../utils/constants';
import EventInRow from '../components/EventComponents/EventInRow';
import { Container, Content } from 'native-base';
import { useTranslation } from 'react-i18next';
import FabButton from '../components/FabButton';

const mapStateToProps = state => ({
    homeProps: {
        banners: state.banners,
        topViewArticles: state.topViewArticles,
        jobArticles: state.jobArticles,
        apartmentArticles: state.apartmentArticles,
        lifestyleArticles: state.lifestyleArticles,
        simArticles: state.simArticles,
        cosmeArticles: state.cosmeArticles,
        churchs: state.churchs,
        events: state.events,
    }
});

let contentComponent = null;

const HomeScreen = connect(mapStateToProps)(function ({ navigation, homeProps, dispatch }) {
    const [state, setState] = useState({
        loading: homeProps.banners.length == 0,
    });

    const [top, setTop] = useState(true);

    const { t } = useTranslation();

    const { loading } = state;

    const toTop = () => {
        if (contentComponent) {
            contentComponent._root.scrollToPosition(0, 0);
        }
    };

    handleSetTop = temp => {
        setTop(temp);
    }

    const appear = ({ nativeEvent }) => {
        if (nativeEvent.contentOffset.y > 50) {
            handleSetTop(false);
        }
        else {
            handleSetTop(true);
        }
    }

    const getBanner = async () => {
        try {
            const [
                bannersResponse,
                topViewArticlesRes,
            ] = await Promise.all([
                fetchBanners(),
                fetchArticles({ order: 'reputation', per_page: 3 }),
            ]);

            dispatch(setPrimary({
                banners: bannersResponse.data._data.mobile_banners,
                topViewArticles: topViewArticlesRes.data._data.articles.data,
            }))

            setState({ ...state, loading: false, });

        }
        catch (error) {
            console.log("Get getPrimaryData error", error)
        }
    };

    const refresh = () => {
        dispatch(resetAll())
        setState({ ...state, loading: true })
        getData()
    };

    const logoPressed = () => {
        toTop()
        refresh()
    }

    useEffect(() => {
        getData();

        const storeState = store.getState();
        let language = storeState.language

        // Get new articles when switch language
        store.subscribe(() => {
            const storeState = store.getState();
            if (storeState.language != language) {
                language = storeState.language;
                refresh();
            }
        })

        // Navigation events
        navigation.addListener('willFocus', payload => {
            if (payload.action.params && payload.action.params.toTop === true) {
                toTop()
            }
        });

    }, []);

    const getData = async () => {
        getBanner()
        try {
            const [
                jobArticlesRes,
                apartmentArticlesRes,
                lifestyleArticlesRes,
                simArticlesRes,
                cosmeArticlesRes,
                churchRes,
                eventRes
            ] = await Promise.all([
                fetchArticles({ type: 'job', per_page: 4 }),
                fetchArticles({ type: 'apartment', per_page: 4 }),
                fetchArticles({ type: 'lifestyle', per_page: 4 }),
                fetchArticles({ type: 'sim', per_page: 4 }),
                fetchArticles({ type: 'cosme', per_page: 4 }),
                fetchTopChurch(),
                fetchEvents({ per_page: 4 })
            ])

            dispatch(setActicles({
                jobArticles: jobArticlesRes.data._data.articles.data,
                apartmentArticles: apartmentArticlesRes.data._data.articles.data,
                lifestyleArticles: lifestyleArticlesRes.data._data.articles.data,
                simArticles: simArticlesRes.data._data.articles.data,
                cosmeArticles: cosmeArticlesRes.data._data.articles.data,
                churchs: churchRes.data,
                events: eventRes.data._data.events.data
            }))
        } catch (error) {
            console.log("===================ERROR================")
            console.log(error)
        }
    };

    return (
        <Container style={styles.container}>



            <AppHeader logoPressed={logoPressed} />

            <Content ref={c => contentComponent = c} onScroll={appear}>
                <Banners banners={homeProps.banners} />

                <DoubleRow
                    uri={type_utils.most_viewed.icon}
                    title={t('type.most_viewed')}
                    data={homeProps.topViewArticles}
                    readMore={{ order: 'reputation' }}
                    reload={() => {}}
                />

                <DoubleRow
                    uri={type_utils.job.icon}
                    title={t('type.job')}
                    data={homeProps.jobArticles}
                    half
                    readMore={{ type: 'job'}}
                    reload={() => {}}
                />

                <DoubleRow
                    uri={type_utils.apartment.icon}
                    title={t('type.apartment')}
                    data={homeProps.apartmentArticles}
                    half
                    readMore={{ type: 'apartment' }}
                    reload={() => {}}
                />

                <DoubleRow
                    uri={type_utils.lifestyle.icon}
                    title={t('type.lifestyle')}
                    data={homeProps.lifestyleArticles}
                    half
                    readMore={{ type: 'lifestyle' }}
                    reload={() => {}}
                />

                <DoubleRow
                    uri={type_utils.cosme.icon}
                    title={t('type.cosme')}
                    data={homeProps.cosmeArticles}
                    half
                    readMore={{ type: 'cosme' }}
                    reload={() => {}}
                />

                <ChurchInRow
                    uri={type_utils.church.icon}
                    title={t('type.church')}
                    data={homeProps.churchs}
                />

                <EventInRow
                    uri={type_utils.event.icon}
                    title={t('type.event')}
                    data={homeProps.events}
                />

                <Footer onPress={toTop} />

            </Content>

            <FabButton onPress={toTop} display={top ? 'none' : null} />

        </Container>
    );
});

HomeScreen.navigationOptions = {
    header: null,
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    }
});



export default HomeScreen;