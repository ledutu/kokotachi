import React, { useState, useEffect } from 'react';
import { StyleSheet, Image, FlatList, TouchableOpacity } from 'react-native';
import DetailMenuHeader from '../components/detailMenuComponent/DetailMenuHeader';
import Footer from '../task/Footer';
import { fetchArticles } from '../utils/api';
import { Container, Content, Card, CardItem, Text, Body, View, Button, ListItem, } from 'native-base';
import { width, type_utils, } from '../utils/constants';
import { imageSource } from '../utils/pureFunction';
import { CancelToken } from '../utils/axios';
import { useTranslation } from 'react-i18next'
import { PangolinText, AppIndicator } from '../utils/commons';
import { format, parseISO } from 'date-fns';

let contentComponent = null

export default function DetailScreen({ navigation }) {

    const [state, setState] = useState({
        loading: false,
        error: false,
        articles: [],
        paginated: null,
        loadMoreDisabled: false,
        cancelTokenSource: CancelToken.source(),
        lastPage: false,
    });

    const { t } = useTranslation();

    const { loading, error, articles, paginated, loadMoreDisabled, cancelTokenSource, lastPage } = state;




    getData = async (page = 1) => {
        setState({ ...state, loading: true, loadMoreDisabled: true, });

        try {

            const type = navigation.getParam('type')
            const slug_category = navigation.getParam('slug')
            const author_id = navigation.getParam('author_id')
            const title = navigation.getParam('title');
            console.log(type);

            const results = await fetchArticles({ type, slug_category, page, author_id, cancelToken: cancelTokenSource.token });

            setState({
                ...state,
                loading: false,
                error: false,
                loadMoreDisabled: false,
                paginated: results.data._data.articles,
                articles: page === 1 ? results.data._data.articles.data :
                    articles.concat(results.data._data.articles.data),
            });

            if (contentComponent && page == 1) {
                setTimeout(() => {
                    contentComponent._root.scrollToPosition(0, 0)
                })
            }
        }
        catch (e) {
            setState({
                loading: false,
                error: true,
            })
        }
    };

    useEffect(() => {
        getData();
    }, []);

    if (!paginated) {
        return AppIndicator
    }

    renderItem = ({ item }) => {
        return (
            <TouchableOpacity onPress={() => navigation.navigate("Detail", { data: item })} activeOpacity={0.9}>

                <Card>
                    <CardItem>
                        <Body style={{ justifyContent: 'space-around' }}>
                            <Image source={imageSource("/storage/" + item.thumbnail)} style={{ width: '100%', height: width, flex: 1 }} />
                            <Text style={styles.title}>{item.title}</Text>
                            <Text style={styles.text}>{format(parseISO(item.approved_at), 'yyyy-MM-dd')}</Text>
                            <View style={styles.interactWrapping}>
                                <Text style={[styles.text, { paddingRight: 10 }]}>{item.like_count} {t('like')}</Text>
                                <Text style={styles.text}>{item.comment_count} {t('comment')}</Text>
                            </View>
                        </Body>
                    </CardItem>
                </Card>

            </TouchableOpacity>
        )
    };

    renderTop = () => {
        return (
            <DetailMenuHeader
                postingNumber={paginated.total}
            />
        )
    };

    renderNext = () => {
        getData(paginated.current_page + 1);
    }

    renderFooter = () => {
        return (
            <View>

                {error && (
                    <View style={styles.center}>
                        <Text>Error</Text>
                    </View>
                )}

                {paginated.current_page >= paginated.last_page && (
                    <View style={[styles.center, { marginVertical: 10 }]}>
                        <Text>Bạn đã ở cuối bài viết</Text>
                    </View>
                )}

                {paginated.current_page < paginated.last_page && (
                    <ListItem noBorder>
                        <Body style={{ flexDirection: 'row', justifyContent: 'center' }}>
                            <Button disabled={loadMoreDisabled} rounded bordered small onPress={renderNext}>
                                <Text>{t('readMore')}</Text>
                            </Button>
                        </Body>
                    </ListItem>
                )}

                <Footer />
            </View>
        )
    };




    return (
        <Container>

            <Content
                style={{ flex: 1, }}
                contentContainerStyle={{ flex: 1, }}
                ref={c => contentComponent = c}
            >

                {paginated.total === 0 && (
                    <View style={{ alignItems: 'center', flex: 1, justifyContent: 'center', }}>
                        <Text style={{ fontSize: 20 }}>Chưa có bài viết nào</Text>
                    </View>
                )}
                {paginated.total !== 0 && (
                    <FlatList
                        data={articles}
                        renderItem={renderItem}
                        keyExtractor={(item, index) => index.toString()}
                        ListHeaderComponent={renderTop}
                        ListFooterComponent={renderFooter}
                        initialNumToRender={3}
                    />
                )}
            </Content>
        </Container>


    );
}

DetailScreen.navigationOptions = ({ navigation }) => {
    const type = navigation.getParam('type');
    const order = 'most_viewed';
    const slug = navigation.getParam('slug');
    const title = navigation.getParam('title');

    return {
        headerRight: <View />,
        headerTitle: <Title type={type ? type : order} title={title} readMore={!title ? true : false} />,
    }
};

const Title = ({ type, title, readMore }) => {
    const { t } = useTranslation();

    if (readMore) {
        return (
            <View style={[{ flexDirection: 'row', }]}>
                <Image source={type_utils[type].icon} resizeMode="contain" style={[Platform.OS === 'ios' && { marginBottom: 10 }, styles.titleImage]} />

                <View style={{ flexDirection: 'column', justifyContent: 'center', }}>
                    <TouchableOpacity activeOpacity={1}>
                        <PangolinText style={styles.typeDisplay}>{t(`type.${type_utils[type].normalDisplay}`)}</PangolinText>
                    </TouchableOpacity>
                </View>
            </View>
        )
    };

    return (
        <View style={[{ flexDirection: 'row' }]}>
            <Image source={type_utils[type].icon} resizeMode="contain" style={[Platform.OS === 'ios' && { marginBottom: 10 }, styles.titleImage]} />

            <View style={{ flexDirection: 'column', justifyContent: 'center' }}>
                {/* // onPress={() => navigation.push('DetailMore', { type, title: t(type_utils[type].display) })} */}

                <TouchableOpacity activeOpacity={1} >
                    <PangolinText style={styles.typeDisplay}>{t(`type.${type_utils[type].normalDisplay}`)}</PangolinText>
                </TouchableOpacity>

                {(title !== ('Job') && title !== ('Apartment') && title !== ('Cosmetic') && type !== 'lifestyle') && (
                    //  onPress={() => navigation.push('DetailMore', { slug, type: category.type, title })}
                    <TouchableOpacity activeOpacity={1}>
                        <PangolinText style={styles.categoryTitle}>{title}</PangolinText>
                    </TouchableOpacity>
                )}

            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    detailContainer: {

        flex: 1,
    },

    container: {
        flex: 1,
    },

    interactWrapping: {
        flexDirection: 'row',
        marginTop: 10,
        justifyContent: 'space-around',
    },

    title: {
        fontSize: 22.5,
        color: '#333333',
        marginVertical: 10
    },
    text: {
        color: 'rgba(51, 51, 51, 0.3)',
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    typeDisplay: {
        fontSize: 20,
        textAlign: 'center',
    },
    categoryTitle: {
        fontSize: 16,
    },
    titleImage: {
        height: 33,
        marginTop: 10
    }
});
