import React  from 'react';
import { View, StyleSheet } from 'react-native';
import SectionItem from '../SectionItem';
import ChurchItem from './ChurchItem';
import PropTypes from 'prop-types';
import { withNavigation } from 'react-navigation';
import { useTranslation } from 'react-i18next'

function ChurchInRow({ navigation, data, uri, title }) {

    ChurchInRow.propTypes = {
        uri: PropTypes.number.isRequired,
        title: PropTypes.string.isRequired,
        data: PropTypes.array.isRequired,
        onPress: PropTypes.func,
    };

    handleGoToList = () => {
        navigation.navigate("ChurchList")
    }

    const centerData = Math.floor(data.length / 2);
    const {t} = useTranslation();
    return (
        <View style={styles.container}>
            <SectionItem
                uri={uri}
                title={title}
                button={t('readMore')}
                onPress={handleGoToList}
                half
            />

            <View style={{ marginTop: 60, flexDirection: 'row', justifyContent: 'space-around', }}>
                <View style={{ flexDirection: 'column' }}>
                    {
                        data.slice(0, centerData).map(item => {
                            return (
                                <ChurchItem
                                    key={item.id}
                                    data={item}
                                    screen="Church"
                                    info={item}
                                    onPress={handleGoToList}
                                />
                            )
                        })
                    }
                </View>
                <View style={{ flexDirection: 'column' }}>
                    {
                        data.slice(centerData).map(item => {
                            return (
                                <ChurchItem
                                    key={item.id}
                                    data={item}
                                    screen="Church"
                                    info={item}
                                    onPress={handleGoToList}
                                />
                            )
                        })
                    }
                </View>
            </View>
        </View>

    );
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        justifyContent: 'space-around',
        marginBottom: 30,
    },

});

export default withNavigation(ChurchInRow);