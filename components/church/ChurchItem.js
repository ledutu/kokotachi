import React, { useState } from 'react';
import { View, Text, Image, TouchableOpacity, StyleSheet, Dimensions } from 'react-native';
import { imageSource } from '../../utils/pureFunction';
import { format, parseISO } from 'date-fns';
import { withNavigation } from 'react-navigation';
import { useTranslation } from 'react-i18next'

const { width } = Dimensions.get('window');

function ChurchItem({ data: { thumbnail_url400x, article, title, }, onPress, navigation, screen, info }) {

    const { t } = useTranslation();
    const [oneLine, setOneLine] = useState(false)

    goToAnotherScreen = () => {
        navigation.push(screen, { data: info });
    };

    handleGetLines = ({ nativeEvent }) => {
        const lines = nativeEvent.lines.length;
        if (lines === 1) {
            setOneLine(true)
        }
    }

    return (
        <View>
            <TouchableOpacity
                style={styles.container}
                activeOpacity={0.9}
                onPress={goToAnotherScreen}
            >
                <Text style={styles.titleStyle} numberOfLines={2} onTextLayout={handleGetLines}>{title}</Text>
                {oneLine && <Text style={{marginBottom: 5}}></Text>}

                <Image
                    source={imageSource(thumbnail_url400x)}
                    style={styles.image}
                />
                <View style={styles.shareAndDatePosting}>
                    <Text style={styles.datePosting}>{format(parseISO(article.created_at), 'yyyy/MM/dd')}</Text>
                    <Text style={styles.textButton} numberOfLines={1} onPress={onPress}>{t('type.church')}</Text>
                </View>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        padding: 15,
        width: width / 2,
    },

    image: {
        height: width / 2,
        width: '100%',
        borderRadius: 10,
    },

    shareAndDatePosting:
    {
        alignItems: 'center',
    },
    textButton: {
        color: 'white',
        fontSize: 15,
        overflow: 'hidden',
        backgroundColor: "#ff2a30",
        paddingVertical: 8,
        paddingHorizontal: 10,
        borderRadius: 16,
        textAlign: 'center'
    },

    datePosting:
    {
        fontSize: 14,
        color: 'rgba(51, 51, 51, 0.3)',
        paddingVertical: 5,
    },
    titleStyle:
    {
        textTransform: 'uppercase',
        color: '#333333',
        fontSize: 17,
        fontWeight: 'bold',
        marginBottom: 5,
    }


})

export default withNavigation(ChurchItem);