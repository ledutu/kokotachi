import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { withNavigation } from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';

class SectionItem extends Component {

    static propTypes = {
        // uri: PropTypes.number,
        title: PropTypes.string.isRequired,
        button: PropTypes.string.isRequired,
    };

    render() {
        const { uri, title, button, onPress, half } = this.props
        return (
            <View style={styles.container}>
                <View style={{ paddingHorizontal: 10 }}>
                    <View style={styles.header}>
                        <View style={styles.wrapIconAndText}>
                            {uri && (
                                <Image
                                    source={uri}
                                    style={styles.icon}
                                />
                            )}

                            <Text style={styles.title} onPress={onPress}>{title}</Text>
                        </View>
                        {half && (
                            <TouchableOpacity style={styles.button} onPress={onPress}>
                                <Text style={styles.more}>{button}</Text>
                                <Icon name={'angle-right'} size={25} color="#0366d6" />
                            </TouchableOpacity>
                        )}

                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        paddingTop: 18,
        backgroundColor: 'rgba(237, 237, 237, 0.5)',
        height: 200,
        top: 0,
        left: 0,
        position: 'absolute',
        width: '100%',
    },
    header: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between',

    },
    wrapIconAndText:
    {
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    icon: {
        height: 40,
        width: 40,
        resizeMode: 'contain',
        marginRight: 5
    },
    title:
    {
        color: 'black',
        fontSize: 20,
        fontWeight: "bold"
    },
    more:
    {
        fontSize: 16,
        color: "#0366d6",
        paddingRight: 5
    },
    button: {
        paddingHorizontal: 10,
        borderWidth: 1,
        borderColor: '#0366d6',
        borderRadius: 20,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
    }


});

export default withNavigation(SectionItem);
