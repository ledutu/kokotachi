import React, { Component } from 'react';
import { useTranslation } from 'react-i18next'
import { View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { type_utils } from '../utils/constants';
import { PangolinText } from '../utils/commons';
import { withNavigation } from 'react-navigation';

function HeaderTitle({ category, navigation, readMore }) {
    const { t } = useTranslation()
    const _styles = StyleSheet.create({
        typeDisplay: {
            fontSize: 20,
            textAlign: 'center',
        },
        categoryTitle: {
            fontSize: 16,
        },
        titleImage: {
            height: 33,
            marginTop: 10
        }
    });

    if (readMore) {
        return (
            <View style={[{ flexDirection: 'row', }]}>
                <Image source={type_utils[category].icon} resizeMode="contain" style={[Platform.OS === 'ios' && { marginBottom: 10 }, _styles.titleImage]} />

                <View style={{ flexDirection: 'column', justifyContent: 'center', }}>
                    <TouchableOpacity activeOpacity={1}>
                        <PangolinText style={_styles.typeDisplay}>{t(`type.${type_utils[category].normalDisplay}`)}</PangolinText>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    return (
        <View style={[{ flexDirection: 'row' }]}>
            <Image source={type_utils[category.type].icon} resizeMode="contain" style={[Platform.OS === 'ios' && { marginBottom: 10 }, _styles.titleImage]} />

            <View style={{ flexDirection: 'column', justifyContent: 'center' }}>
                {category.type != 'lifestyle' && (
                    <TouchableOpacity activeOpacity={0.9} onPress={() => navigation.push('DetailMore', { type: category.type, title: t(type_utils[category.type].display) })}>
                        <PangolinText style={_styles.typeDisplay}>{t(`type.${type_utils[category.type].normalDisplay}`)}</PangolinText>
                    </TouchableOpacity>
                )}

                {category.title && (
                    <TouchableOpacity activeOpacity={0.9} onPress={() => navigation.push('DetailMore', { slug: category.slug, type: category.type, title: category.title })}>
                        <PangolinText style={_styles.categoryTitle}>{category.title}</PangolinText>
                    </TouchableOpacity>
                )}
            </View>
        </View>
    )
};

export default withNavigation(HeaderTitle);
