import React from 'react';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import EventItem from './EventItem';
import SectionItem from '../SectionItem';
import { withNavigation } from 'react-navigation';
import { useTranslation } from 'react-i18next'
import { navigate } from '../../utils/navigation';

function EventInRow({navigation, data, uri, title}) {

    EventInRow.propTypes = {
        data: PropTypes.array.isRequired,
        title: PropTypes.string.isRequired,
        uri: PropTypes.number.isRequired,
    };

    const centerData = Math.floor(data.length / 2);
    const {t} = useTranslation();

    return (
        <View style={styles.container}>
            <SectionItem
                uri={uri}
                title={title}
                button={t('readMore')}
                half
                onPress={() => navigate('EventList', { title: 'Event' })}
            />

            <View style={{ marginTop: 60, flexDirection: 'row', justifyContent: 'space-around', }}>
                <View style={{ flexDirection: 'column' }}>
                    {
                        data.slice(0, centerData).map(item => {
                            return (
                                <EventItem
                                    key={item.id}
                                    data={item}
                                    info={item}
                                    handleSwitchScreen={() => navigate("Event", {data: item})}
                                    onPress={
                                        () => {
                                            navigation.navigate(
                                                'EventList',
                                                {
                                                    title: item.title,
                                                    type: item.type,
                                                    slug: item.slug,
                                                }
                                            )
                                        }
                                    }
                                />
                            )
                        })
                    }
                </View>
                <View style={{ flexDirection: 'column' }}>
                    {
                        data.slice(centerData).map(item => {
                            return (
                                <EventItem
                                    key={item.id}
                                    data={item}
                                    info={item}
                                    handleSwitchScreen={() => navigate("Event", {data: item})}
                                    onPress={
                                        () => {
                                            navigation.navigate(
                                                'EventList',
                                                {
                                                    title: item.title,
                                                    type: item.type,
                                                    slug: item.slug,
                                                }
                                            )
                                        }
                                    }
                                />
                            )
                        })
                    }
                </View>
            </View>
        </View>

    );
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        justifyContent: 'space-around',
        marginBottom: 60,
    },

});

export default withNavigation(EventInRow);