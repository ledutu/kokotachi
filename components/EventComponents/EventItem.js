import React, { useState } from 'react';
import { View, Text, Image, TouchableOpacity, StyleSheet, Dimensions } from 'react-native';
import { withNavigation } from 'react-navigation';
import PropTypes from 'prop-types';
import { imageSource } from '../../utils/pureFunction';
import { format, parseISO } from 'date-fns';
const { width } = Dimensions.get('window');

function EventItem({ data, onPress, handleSwitchScreen }) {

    EventItem.propTypes = {
        info: PropTypes.object.isRequired,
    };

    const [oneLine, setOneLine] = useState(false);

    handleGetLines = ({ nativeEvent }) => {
        const lines = nativeEvent.lines.length;
        if (lines === 1) {
            setOneLine(true);

        }
    }

    const {
        image_path,
        article: {
            created_at,
            title,
        },
    } = data;
    return (
        <View>
            <TouchableOpacity
                style={styles.container}
                activeOpacity={0.9}
                onPress={handleSwitchScreen}
            >
                <Text
                    numberOfLines={2}
                    onTextLayout={handleGetLines}
                    style={styles.titleStyle}
                >{title}</Text>

                {oneLine && (
                    <Text style={{marginBottom: 5,}}></Text>
                )}

                <Image
                    source={imageSource(image_path)}
                    style={styles.image}
                />
                <View style={styles.shareAndDatePosting}>
                    <Text style={styles.datePosting}>{format(parseISO(created_at), 'yyyy/MM/dd')}</Text>
                    <Text style={styles.textButton} numberOfLines={1} onPress={onPress}>Sự kiện</Text>
                </View>
            </TouchableOpacity>

        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        padding: 15,
        width: width / 2,
    },

    image: {
        height: width / 2,
        width: '100%',
        borderRadius: 10,
    },

    shareAndDatePosting:
    {
        alignItems: 'center',
    },
    textButton: {
        color: 'white',
        fontSize: 15,
        overflow: 'hidden',
        backgroundColor: "#ff2a30",
        paddingVertical: 8,
        paddingHorizontal: 10,
        borderRadius: 16,
        textAlign: 'center'
    },

    datePosting:
    {
        fontSize: 14,
        color: 'rgba(51, 51, 51, 0.3)',
        paddingVertical: 5,
    },
    titleStyle:
    {
        textTransform: 'uppercase',
        color: '#333333',
        fontSize: 17,
        fontWeight: 'bold',
        marginBottom: 5,
    }
})

export default withNavigation(EventItem);