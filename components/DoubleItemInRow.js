import React, { useState } from 'react';
import { View, Text, Image, TouchableOpacity, StyleSheet, Dimensions } from 'react-native';
import { withNavigation } from 'react-navigation';
import PropTypes from 'prop-types';
import { imageSource } from '../utils/pureFunction';
import { format, parseISO } from 'date-fns';

const { width } = Dimensions.get('window');

function DoubleItemInRow({ info, reload, data, half, onPress, navigation, type, slug }) {

    const [oneLine, setOneLine] = useState(false)

    DoubleItemInRow.propTypes = {
        info: PropTypes.object.isRequired,
        data: PropTypes.object,
        half: PropTypes.bool,
        onPress: PropTypes.func,
    }

    goToAnotherScreen = () => {
        navigation.navigate('Detail', { data: info });
        reload();
    };

    handleGoToList = () => {
        onPress(type, slug, data.category.title);
    };

    handleGetLines = ({ nativeEvent }) => {
        const lines = nativeEvent.lines.length;
        if (lines === 1) {
            setOneLine(true)
        }
    }


    const {
        category,
        thumbnail,
        approved_at,
        title,
    } = data;

    return (
        <View>
            <TouchableOpacity
                style={[styles.container, half ? styles.ahalf : styles.full]}
                activeOpacity={0.9}
                onPress={goToAnotherScreen}
            >
                <Text style={styles.titleStyle} numberOfLines={2} onTextLayout={handleGetLines}>{title}</Text>
                {half && oneLine && (
                    <Text style={{ marginBottom: 5 }}></Text>
                )}  

                <Image
                    source={imageSource("/storage/" + thumbnail)}
                    style={[styles.image, half ? { height: width / 2, } : { height: width, }]}
                />

                {!half && (
                    <View style={styles.shareAndDatePosting}>
                        <Text style={styles.textButton} numberOfLines={half ? 2 : 1} onPress={this.handleGoToList}>{category.title}</Text>
                        <Text style={styles.datePosting}>{format(parseISO(approved_at), 'yyyy/MM/dd')}</Text>
                    </View>
                )}
                {half && (
                    <View style={styles.shareAndDatePostingHalf}>
                        <Text style={styles.datePosting}>{format(parseISO(approved_at), 'yyyy/MM/dd')}</Text>
                        <Text style={styles.textButton} onPress={this.handleGoToList}>{category.title}</Text>
                    </View>
                )}


            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        padding: 15,
        justifyContent: 'space-around',
    },

    ahalf: {
        width: width / 2,
        justifyContent: 'space-around',
    },
    full: {
        width: "100%",
    },

    image: {
        width: '100%',
        borderRadius: 10,

    },
    shareAndDatePosting:
    {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 5
    },
    textButton: {
        color: 'white',
        fontSize: 15,
        overflow: 'hidden',
        backgroundColor: "#ff2a30",
        paddingVertical: 8,
        paddingHorizontal: 10,
        borderRadius: 16,
        textAlign: 'center',
    },

    datePosting:
    {
        fontSize: 14,
        color: 'rgba(51, 51, 51, 0.3)',
        paddingVertical: 5
    },
    titleStyle:
    {
        textTransform: 'uppercase',
        color: '#333333',
        fontSize: 17,
        fontWeight: 'bold',
        marginBottom: 5,

    },
    shareAndDatePostingHalf: {
        alignItems: 'center',
    }
})

export default withNavigation(DoubleItemInRow);