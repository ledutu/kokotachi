import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import SectionItem from './SectionItem';
import DoubleItemInRow from './DoubleItemInRow';
import PropTypes from 'prop-types';
import { withNavigation } from 'react-navigation';
import { useTranslation } from 'react-i18next'

function DoubleRow({ navigation, data, uri, title, half, readMore, reload }) {

    DoubleRow.propTypes = {
        data: PropTypes.array.isRequired,
        title: PropTypes.string.isRequired,
        uri: PropTypes.number,
        half: PropTypes.bool,
        reload: PropTypes.func,
    };

    handleGoToList = (type, slug, title) => {
        navigation.navigate('DetailMore', { ...readMore, slug, type, title});
    };  

    handleGoToListForReadMore = () => {
        navigation.navigate('DetailMore', { ...readMore })
    }

    const centerData = Math.floor(data.length / 2);
    const { t } = useTranslation();

    return (
        <View style={styles.container}>

            <SectionItem
                uri={uri ? uri : null}
                title={title}
                button={t('readMore')}
                half={half}
                onPress={handleGoToListForReadMore}
            />

            {half && (
                <View style={{ marginTop: 60, flexDirection: 'row', justifyContent: 'space-around', }}>
                    <View style={{ flexDirection: 'column' }}>
                        {
                            data.slice(0, centerData).map(item => {
                                return (
                                    <DoubleItemInRow
                                        key={item.id}
                                        data={item}
                                        title={item.category.title}
                                        info={item}
                                        half
                                        onPress={handleGoToList}
                                        reload={reload}
                                        slug={item.category.slug}
                                        type={item.category.type}
                                    />
                                )
                            })
                        }
                    </View>
                    <View style={{ flexDirection: 'column' }}>
                        {
                            data.slice(centerData).map(item => {
                                return (
                                    <DoubleItemInRow
                                        key={item.id}
                                        data={item}
                                        info={item}
                                        title={item.category.title}
                                        half
                                        onPress={handleGoToList}
                                        reload={reload}
                                        slug={item.category.slug}
                                        type={item.category.type}
                                    />
                                )
                            })
                        }
                    </View>
                </View>
            )}

            {!half && (
                <View style={{ marginTop: 60 }}>
                    {
                        data.map(item => {
                            return (
                                <DoubleItemInRow
                                    key={item.id}
                                    data={item}
                                    info={item}
                                    half={false}
                                    onPress={handleGoToList}
                                    title={item.category.title}
                                    reload={reload} 
                                    slug={item.category.slug}
                                    type={item.category.type}
                                />
                            )
                        })
                    }
                </View>
            )}

        </View>

    );
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        justifyContent: 'space-around',
        marginBottom: 20,
    },

});

export default withNavigation(DoubleRow);