import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import PropTypes from 'prop-types';

export default function DetailMenuHeader({
    postingNumber,
}) {

    return (
        <View style={styles.container}>
            <View>
                <Text style={styles.postingNumber}>{postingNumber} Bài viết</Text>
            </View>
        </View>
    );
};

DetailMenuHeader.propTypes = {
    postingNumber: PropTypes.number,
};

const styles = StyleSheet.create({
    container: {
        paddingTop: 27,
        marginHorizontal: 15,
        paddingBottom: 10,
    },
    postingNumber: {
        color: 'rgba(51, 51, 51, 0.3)',
        fontSize: 15,
    },

});
