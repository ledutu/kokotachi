import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export default function FabButton({ onPress, display }) {

    return (
        <View style={{display}}>
            <TouchableOpacity style={styles.fabButton} activeOpacity={0.9} onPress={onPress}>
                <Icon
                    name="arrow-up"
                    size={25}
                    color="white"
                />
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    fabButton: {
        position: 'absolute',
        paddingHorizontal: 10,
        paddingVertical: 10,
        alignItems: 'center',
        justifyContent: 'center',
        right: 30,
        bottom: 60,
        backgroundColor: '#ff2a30',
        borderRadius: 50
    }
})
